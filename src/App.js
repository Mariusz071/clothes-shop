import React from 'react';
import { Route } from 'react-router'
import { Switch } from "react-router";

import { Navbar } from './components/navBar';
import { routingConfig } from "./resources/routingConfig";

import 'normalize.css'
import './App.css'

export const App = () => (
    <div className='App'>
        <Navbar />
        <Switch>
            {routingConfig.map((page, idx) => {
                const { exact, path, component } = page
                return <Route key={idx} exact={exact} path={path} component={component} />
            }
            )}
        </Switch>
    </div>
);
