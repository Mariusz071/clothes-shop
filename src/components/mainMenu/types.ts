declare type Size = 'large' | 'medium' | 'small'

export interface MenuItemType {
    title: string
    subTitle: string
    id: string
    size?: Size
}
