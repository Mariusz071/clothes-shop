import styled from 'styled-components'

export const MainMenuContainer = styled.div`
    flex-wrap: wrap;
    display: flex;
    justify-content: space-between;
`
