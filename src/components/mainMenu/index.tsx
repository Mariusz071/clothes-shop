import React from 'react'

import { MenuItem } from 'src/components/menuItem'
import { MenuItemType } from './types'
import { MainMenuContainer } from './index.styles'

const menuConfig = [
    {
        id: 'hats',
        title: 'Hats',
        subTitle: 'Shop now',
    },
    {
        id: 'jackets',
        title: 'Jackets',
        subTitle: 'Shop now',
    },
    {
        id: 'sneakers',
        title: 'Sneakers',
        subTitle: 'Shop now',
    },
    {
        id: 'women',
        title: 'Women',
        subTitle: 'Shop now',
        size: 'large' as const,
    },
    {
        id: 'men',
        title: 'Men',
        subTitle: 'Shop now',
        size: 'large' as const,
    },
]
export const MainMenu: React.FC = () => {
    const [menuItems, setMenuItems] = React.useState<MenuItemType[]>(menuConfig)
    return (
        <MainMenuContainer>
            {menuItems.map((item) => (
                <MenuItem item={item} key={item.id} />
            ))}
        </MainMenuContainer>
    )
}
