import styled from 'styled-components'

export const CollectionItemContainer = styled.div`
    width: 22%;
    display: flex;
    flex-direction: column;
    height: 350px;
    align-items: center;

    .image {
        background: ${(props) => `url(${props.backgroundImage})`} no-repeat center;
        width: 100%;
        height: 95%;
        margin: 0 0 5px 0;
    }

    .footer {
        width: 100%;
        height: 5%;
        display: flex;
        justify-content: space-between;
        font-size: var(--text-medium);
    }
`
