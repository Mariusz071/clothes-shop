import React from 'react'
import { CollectionItemContainer } from './index.styles'
import { ShopItem } from '../previewItem/types'

interface Props {
    item: ShopItem
}

export const CollectionItem: React.FC<Props> = ({ item }) => {
    const { imgUrl, name, price } = item
    return (
        <CollectionItemContainer backgroundImage={imgUrl}>
            <div className='image'></div>
            <div className='footer'>
                <span className='footer__name'>{name}</span>
                <span className='footer__price'>{price}</span>
            </div>
        </CollectionItemContainer>
    )
}
