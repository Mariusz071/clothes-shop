import styled from 'styled-components'

export const CollectionPreviewContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 0 30px 0;

    .title {
        font-size: var(--text-big);
        margin: 0 0 25px 0;
    }

    .preview {
        display: flex;
        justify-content: space-between;
    }

    .content {
        display: flex;
        justify-content: space-between;
    }
`
