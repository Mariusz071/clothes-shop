import React from 'react'

import { CollectionPreviewContainer } from './index.styles'
import { Collection } from './types'
import { CollectionItem } from 'src/components/collectionItem'

interface Props {
    collection: Collection
}

export const CollectionPreview: React.FC<Props> = ({ collection }) => {
    const { title, items } = collection
    const previewItems = items.slice(0, 4)
    return (
        <CollectionPreviewContainer>
            <h2 className={'title'}>{title}</h2>
            <div className={'content'}>
                {previewItems.map((item) => (
                    <CollectionItem key={item.id} item={item} />
                ))}
            </div>
        </CollectionPreviewContainer>
    )
}
