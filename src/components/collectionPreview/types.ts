export interface ShopItem {
    id: string
    name: string
    price: number
    imgUrl?: string
}

export interface Collection {
    id: number
    title: string
    path: string
    items: ShopItem[]
}
