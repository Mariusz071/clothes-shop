import styled from 'styled-components'

export const CustomButtonWrapper = styled.button`
    min-width: 165px;
    width: auto;
    height: 50px;
    letter-spacing: 0.5px;
    line-height: 50px;
    padding: 0 35px;
    background-color: var(--black);
    color: var(--white);
    text-transform: uppercase;
    font-family: 'Open Sans Condensed', sans-serif;
    border: none;
    cursor: pointer;
    transition: all 0.1s;
    outline: none;

    &:hover {
        background-color: var(--white);
        color: var(--black);
        border: 1px solid var(--black);
    }
`
