import React from 'react'
import { CustomButtonWrapper } from './index.styles'

interface Props {
    onClick?: any
    children: React.ReactChild
    type?: 'submit'
}

export const CustomButton: React.FC<Props> = ({ children, onClick }) => {
    return (
        <CustomButtonWrapper onClick={onClick} className='custom-btn'>
            {children}
        </CustomButtonWrapper>
    )
}
