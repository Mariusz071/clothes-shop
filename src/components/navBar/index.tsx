import React from 'react'
import { NavbarContainer } from './index.styles'
import { Link } from 'react-router-dom'
import { ReactComponent as MainLogo } from 'src/resources/images/logo.svg'

export const Navbar = () => {
    return (
        <NavbarContainer>
            <Link to='/' className='logo'>
                <MainLogo />
            </Link>
            <div className='menu-items'>
                <Link className='menu-items__item' to='/shop'>
                    Shop
                </Link>
                <Link className='menu-items__item' to='/contact'>
                    Contact
                </Link>
                <Link className='menu-items__item' to='/login'>
                    Log In
                </Link>
            </div>
        </NavbarContainer>
    )
}
