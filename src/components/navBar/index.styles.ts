import styled from 'styled-components'

export const NavbarContainer = styled.div`
    height: 4.5em;
    display: flex;
    justify-content: space-between;

    .logo {
        width: 4.5em;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .menu-items {
        flex: 1 1 auto;
        display: flex;
        justify-content: flex-end;
        align-items: center;

        &__item {
            padding: 0.5em;
        }
    }
`
