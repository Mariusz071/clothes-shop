import styled from 'styled-components'

export const MenuItemContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-width: 30%;
    flex-wrap: wrap;
    margin: 0.5em;
    flex: 1 1 auto;
    height: 280px;
    overflow: hidden;

    &.large {
        height: 420px;
    }

    .bg-img {
        width: 100%;
        height: 100%;
        background: ${(props) => `url(${props.backgroundImage})`} no-repeat center;
        background-size: cover;
        transition: transform 0.3s;

        &:hover {
            transform: scale(1.1);
        }
    }

    .content {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        width: 150px;
        height: 150px;
        background: rgba(255, 255, 255, 0.5);
        color: #313632;
        transition: all 0.2s;
        position: absolute;

        &:hover {
            background: rgba(255, 255, 255, 0.8);
            cursor: pointer;
        }

        &__heading {
            margin: 0 0 0.5em 0;
        }

        &__sub-heading {
            text-transform: uppercase;
        }
    }
`
