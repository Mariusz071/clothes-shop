import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'

import { MenuItemContainer } from './index.styles'
import { MenuItemType } from '../mainMenu/types'
import { getBgImage } from '../../resources/utils'

interface Props extends RouteComponentProps {
    item: MenuItemType
}

const MenuItemComponent: React.FC<Props> = ({ item, history }) => {
    const { title, subTitle, id, size = '' } = item
    const backgroundImage = getBgImage(id)

    return (
        <MenuItemContainer className={`menu-item ${size}`} backgroundImage={backgroundImage}>
            <div className={'bg-img'} />
            <div className={'content'} onClick={() => history.push(`/${id}`)}>
                <h1 className={'content__heading'}>{title}</h1>
                <span className={'content__sub-heading'}>{subTitle}</span>
            </div>
        </MenuItemContainer>
    )
}

export const MenuItem = withRouter(MenuItemComponent)
