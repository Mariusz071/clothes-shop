import React from 'react'

import { RegisterFormContainer } from './index.styles'

export const RegisterForm = (prop) => {
    return (
        <RegisterFormContainer>
            <h2>Register form</h2>
        </RegisterFormContainer>
    )
}
