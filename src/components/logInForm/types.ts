export interface FormValues {
    email: string
    password: string
}

export interface FieldConfig {
    fieldName: string
    type: string
    label: string
    isRequired?: boolean
}
