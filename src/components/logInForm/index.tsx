import React from 'react'

import { LogInFormContainer } from './index.styles'
import { FormValues, FieldConfig } from './types'
import { FormInput } from '../formInput'
import { CustomButton } from '../customButton'

const initialValues: FormValues = {
    email: '',
    password: '',
}

interface Props {
    config: FieldConfig[]
}

export const LogInForm: React.FC<Props> = ({ config }) => {
    const [values, setValues] = React.useState<FormValues>(initialValues)

    const onChangeHandler = (e: React.BaseSyntheticEvent<InputEvent>) => {
        const { name, value } = e.currentTarget
        setValues({ ...values, [name]: value })
    }

    const onSubmitHandler = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault()
        setValues(initialValues)
    }

    return (
        <LogInFormContainer>
            <h2>I already have an account.</h2>
            <span>Sign in with your email and password.</span>
            <form onSubmit={onSubmitHandler}>
                {config.map((field, idx) => {
                    const { fieldName, type, label } = field
                    return (
                        <FormInput
                            key={idx}
                            name={fieldName}
                            type={type}
                            label={label}
                            value={values[fieldName]}
                            onChange={onChangeHandler}
                        />
                    )
                })}
                <CustomButton type='submit'>Sign In</CustomButton>
            </form>
        </LogInFormContainer>
    )
}
