import React from 'react'

import { PreviewItemContainer } from './index.styles'
import { ShopItem } from './types'

interface Props {
    item: ShopItem
}

export const PreviewItem: React.FC<Props> = ({ item }) => {
    return (
        <PreviewItemContainer>
            <div className={'mage'}></div>
            <h3 className={'title'}>{item.name}</h3>
        </PreviewItemContainer>
    )
}
