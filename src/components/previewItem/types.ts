export interface ShopItem {
    id: string
    name: string
    price: number
    imgUrl?: string
}
