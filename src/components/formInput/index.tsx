import React from 'react'
import cn from 'classnames'
import { FormInputWrapper } from './index.styles'

interface Props {
    onChange: (e: React.BaseSyntheticEvent<InputEvent>) => void
    name: string
    type: string
    value?: string
    isRequired?: boolean
    label?: string
}
export const FormInput: React.FC<Props> = ({ onChange, label, name, type, value, isRequired }) => {
    return (
        <FormInputWrapper>
            <input
                className='form-input'
                name={name}
                type={type}
                value={value}
                onChange={onChange}
                required={isRequired}
            />
            {label && <label className={cn('label', value.length && 'shrink')}>{label}</label>}
        </FormInputWrapper>
    )
}
