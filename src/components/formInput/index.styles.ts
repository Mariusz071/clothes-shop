import styled from 'styled-components'

export const FormInputWrapper = styled.div`
    position: relative;
    margin: 45px 0;

    .form-input {
        border: 1px solid black;
        background: none;
        background-color: var(--white);
        color: var(--gray);
        font-size: var(--text-medium);
        padding: 10px 10px 10px 5px;
        display: block;
        width: 100%;
        border: none;
        border-radius: 0;
        border-bottom: 1px solid var(--gray);
        margin: 25px 0;

        &:focus {
            outline: none;
        }

        &:focus ~ .label {
            top: -14px;
            font-size: 12px;
            color: var(--black);
        }
    }

    .label {
        color: var(--black);
        font-size: var(--text-medium);
        font-weight: normal;
        position: absolute;
        pointer-events: none;
        left: 5px;
        top: 10px;
        transition: 300ms ease all;

        &.shrink {
            top: -14px;
            font-size: var(--text-small);
            color: var(--black);
        }
    }

    input[type='password'] {
        letter-spacing: 0.3em;
    }
`
