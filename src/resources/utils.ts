import hatsBg from 'src/resources/images/hats.png'
import sneakersBg from 'src/resources/images/sneakers.png'
import menBg from 'src/resources/images/men.png'
import womenBg from 'src/resources/images/women.png'
import jacketsBg from 'src/resources/images/jackets.png'

const backgroundImages = {
    hats: hatsBg,
    men: menBg,
    women: womenBg,
    sneakers: sneakersBg,
    jackets: jacketsBg,
}

export const getBgImage = (id: string) => backgroundImages[id]
