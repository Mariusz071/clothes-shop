import { RoutingConfig } from './types'

import { HatsPage } from 'src/pages/hats/HatsPage'
import { JacketsPage } from 'src/pages/jackets/JacketsPage'
import { HomePage } from 'src/pages/homePage'
import { ShopPage } from 'src/pages/shopPage'
import { LoginPage } from 'src/pages/loginPage/LoginPage'

export const routingConfig: RoutingConfig = [
    {
        path: '/',
        exact: true,
        component: HomePage,
    },
    {
        path: '/shop',
        component: ShopPage,
    },
    {
        path: '/hats',
        component: HatsPage,
    },
    {
        path: '/jackets',
        component: JacketsPage,
    },
    {
        path: '/login',
        component: LoginPage,
    },
]
