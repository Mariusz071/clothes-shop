import React from 'react'
import { HomePageContainer } from './index.styles'
import { MainMenu } from 'src/components/mainMenu'

export const HomePage = () => (
    <HomePageContainer>
        <MainMenu />
    </HomePageContainer>
)
