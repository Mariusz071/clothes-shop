import React from 'react'

import { LoginPageContainer } from './LoginPage.styles'
import { LogInForm } from 'src/components/logInForm'
import { RegisterForm } from 'src/components/registerForm'
import { FieldConfig } from 'src/components/logInForm/types'

const logInFormConfig: FieldConfig[] = [
    {
        fieldName: 'email',
        type: 'email',
        isRequired: true,
        label: 'Email',
    },
    {
        fieldName: 'password',
        type: 'password',
        isRequired: true,
        label: 'Password',
    },
]

export const LoginPage = (props) => {
    return (
        <LoginPageContainer>
            <LogInForm config={logInFormConfig} />
            <RegisterForm />
        </LoginPageContainer>
    )
}
