interface ShopItem {
    id: string
    name: string
    price: number
    imgUrl?: string
}

interface Collection {
    id: number
    title: string
    path: string
    items: ShopItem[]
}

export declare type Collections = Collection[]
