import React from 'react'

import { SHOP_DATA } from 'src/resources/mockData'
import { Collections } from './types'
import { CollectionPreview } from 'src/components/collectionPreview'

export const ShopPage = (props) => {
    const [collections, setCollections] = React.useState<Collections>(SHOP_DATA)
    return (
        <div>
            <h2>shop data</h2>
            {collections.map((collection) => (
                <CollectionPreview key={collection.id} collection={collection} />
            ))}
        </div>
    )
}
